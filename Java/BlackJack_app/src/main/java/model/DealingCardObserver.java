package model;

/**
 * Observer.
 */
public interface DealingCardObserver {
  public void cardDealingEvent(Player p);
}
