# RecipeBook 

### Source code:
 `src/`  

**Under source folder**:
* `main/java/RecipeBook` --> **application**
1. `Book.java` 
2. `ConsoleUI.java`
3. `Ingredient.java`
4. `IngredientNameStrategy.java`
5. `Main.java` (**You start from here**. Open this file, scroll down to the bottom and run `main()` method)
6. `MaxPriceStrategy.java`
7. `Recipe.java`
8. `RecipeNameStrategy.java`
9. `Strategy.java`
* `test/java/RecipeBook` --> **test cases**  
1. `TestCases.java` (**You run test cases here**)  

