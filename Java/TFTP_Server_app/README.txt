TFTP Server
Submission By:  Qingqing Dai (qd222ab):
**************************************************************************************************************
To run the file  :

1- Open WSL command menu

2- Navigate to "src" folder

3- Write "javac TFTPServer.java"

4- Write "java TFTPServer"

5- Open Tftpd64 (The tool used for this assignment to send and receive the data utilising TFTP) and use it


6- DO NOT delete zero.txt (either of them) or Deny.txt because they are used as fast way
to see the error response implementation.
