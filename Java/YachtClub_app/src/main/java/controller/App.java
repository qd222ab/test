package controller;

/** Responsible for staring the application. */
public class App {
  /** Application starting point. */
  public static void main(String[] args) {
    MainController controller = new MainController();
    controller.appStarter();
  }
}
