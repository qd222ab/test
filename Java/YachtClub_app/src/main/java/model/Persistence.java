package model;

import java.util.List;

/**
 * interface Persistence.
 */
public interface Persistence {
  public List<Member> getMembers();
}
