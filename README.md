# Qingqing Dai
## _My portfolio & and coding experience so far_


In this Repo you shall see some of the code that I wrote during my time at LNU (Linnaeus University).
My experience with coding has been mostly within the academic standard of the university, in terms of assignments, exams, seminars, presentations, reports and teammwork.


I have had an experience coding in:
- Java
- Python
- C(Just the simplistic basics)
- Assembly(Just the simplistic basics)
- JavaScript(Just the simplistic basics)
- HTML + CSS
- mysql


I understand and have practiced some of the mainstream areas of coding such as:
- Object oriented programming
- Functional programming
- Machine Learning
- Multithreaded proccesses
- GitLab
- Software enginnering
- Scrum
- Client-Server communication
- Networking protocols
- Intercation between backend and frontend in software.


I have a theoritical knowledge along side my hands-on coding experience provided by the university syllabus.

Thus, important ideas such as code security, clarity of code, clarity of comments of the code, defensive programming, abstraction, etc. I have understood and practiced these concepts.

I have coded alone and with teams, so I fully comprehend the need to be able to carry one's own weight. I also understand the importance of communicating your ideas, taking and providing constructive criticism.

I am passionate about coding, and I am always willing to take the time to learn new concepts and adapt to the newest changes.

In this repo you will see some small apps that are in one file, and larger scale projects that are in a directory of their own.
